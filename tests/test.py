import unittest

from app.main import hello_pipe


class Test(unittest.TestCase):
    # dummy test
    def test_app_success(self):
        assert 'Pipes are awesome!' == hello_pipe()
